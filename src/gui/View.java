package gui;
import java.util.ArrayList;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.Queue;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import simuator.Client;
import simuator.Shop;
import simuator.ShopQueue;

public class View extends JFrame{
	
	private JPanel jp = new JPanel();
	private static JScrollPane pane1 = new JScrollPane();
	private static JScrollPane pane2 = new JScrollPane();
	private static JScrollPane pane3 = new JScrollPane();
	private static JScrollPane pane4 = new JScrollPane();
	private static JScrollPane paneIndex = new JScrollPane();
	public TextField txtTime = new TextField(5);
	JTextField txtServed = new JTextField(5);
	private FlowLayout layout;
	int minService =0;
	int maxService =0;
	//ArrayList<ShopQueue> queues = new ArrayList<>();
	JList<String> clientsQue1;
	JList<String> clientsQue2;
	JList<String> clientsQue3;
	JList<String> clientsQue4;
	//Shop newSh = new Shop(queues,4,0,0,0,0,0);
	String[] clienti1 = new String[10];
	String[] clienti2 = new String[10];
	JTextField avgWait = new JTextField(5);
	JTextField tmSimtxt = new JTextField(5);
	JTextField minArrivalTxt = new JTextField(5);
	JTextField maxArrivalTxt = new JTextField(5);
	JTextField peakTxt = new JTextField(5);
	
	String[] clienti3 = new String[10];
	String[] clienti4 = new String[10];
	
	JTextArea textArea = new JTextArea(15, 50);
	
	public View(JPanel jp1) {
		jp=jp1;

		
		JList<String> clientsQue4;
		JList<Integer> indexes;
			
		
		JButton startBtn = new JButton("Start");
		JButton prepareBtn = new JButton("Prepare");
		startBtn.setBounds(150, 220, 100,25);
		prepareBtn.setBounds(150, 265, 100,25);
	
		 layout = new FlowLayout();
		 setLayout(layout);	
		 
		
	

		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		JPanel p4 = new JPanel();
		Integer[] indexz= new Integer[11];
		for(int i=1;i<=10;i++) {
			indexz[i]=i;
		}
		
		indexes= new JList<Integer>(indexz);
		paneIndex = new JScrollPane(indexes);
		
		clientsQue1 = new JList<String>(clienti1);
		 pane1 = new JScrollPane(clientsQue1);
		clientsQue1.setVisibleRowCount(10);
		clientsQue2 = new JList<String>(clienti2);
		 pane2 = new JScrollPane(clientsQue2);
		clientsQue2.setVisibleRowCount(10);
		clientsQue3 = new JList<String>(clienti3);
		 pane3 = new JScrollPane(clientsQue3);
		clientsQue3.setVisibleRowCount(10);
		clientsQue4 = new JList<String>(clienti4);
		 pane4 = new JScrollPane(clientsQue4);
			clientsQue4.setVisibleRowCount(10);
		pane1.setBounds(0, 100, 880, 200);
		pane2.setBounds(0, 0, 880, 250);
		pane3.setBounds(0, 0, 880, 300);
		pane4.setBounds(0, 0, 880, 300);
		
		//frm.add(pane);
		 paneIndex.setPreferredSize(new Dimension(40,195));
        pane1.setPreferredSize(new Dimension(100,200));
        pane1.setAlignmentX(JScrollPane.CENTER_ALIGNMENT);
        pane2.setPreferredSize(new Dimension(100,195));
        pane3.setPreferredSize(new Dimension(100,200));
        pane4.setPreferredSize(new Dimension(100,200));
        pane1.setBackground(Color.YELLOW);
        pane2.setBackground(Color.CYAN);
        pane3.setBackground(Color.CYAN);
        pane4.setBackground(Color.CYAN);
        jp.add(paneIndex);
		jp.add(pane1);
		jp.add(pane2);
		jp.add(pane3);
		jp.add(pane4);
		jp.add(startBtn);
		
		layout.setAlignment(FlowLayout.LEFT);
		
		//p2.add(startBtn);
		JLabel lblTime = new JLabel("    Current time :   ");
		JLabel lblServed = new JLabel("    Clients served :  ");
		 
		
		JTextField txtMinService = new JTextField(5);
		JTextField txtMaxService = new JTextField(5);
		JLabel lblMinS = new JLabel("Min service time :");
		JLabel lblMaxS = new JLabel("  Max service time :");
		JLabel avgW = new JLabel("  Average waiting :");
		//JLabel minArv = new JLabel("Minimum arrival interval :");
		//JLabel maxArv = new JLabel("Maximum arrival interval :");
		JLabel peak = new JLabel("Peak time :");
		JLabel tmSim = new JLabel("  Time of simulation:");
		
		
		p2.add(lblMinS);
		p2.add(txtMinService);
		p2.add(lblMaxS);
		p2.add(txtMaxService);
		p2.add(tmSim);
		p2.add(tmSimtxt);
		//p2.add(minArv);
		//p2.add(minArrivalTxt);
		//p2.add(maxArv);
		//p2.add(maxArrivalTxt);
		p3.add(peak);
		p3.add(peakTxt);
		p3.add(lblTime);
		p3.add(txtTime);
		p3.add(lblServed);
	    p3.add(txtServed);
	    p3.add(avgW);
	    p3.add(avgWait);
	    p4.add(textArea);
	    

	    this.setSize(750,650);
        this.setLocationRelativeTo(null);
        this.setVisible(true); 
        add(jp);
        add(p2);
        add(p3);
        add(p4);
        
        startBtn.addActionListener(new ActionListener() {
        		
        		public void actionPerformed(ActionEvent e) {
         int timeSim = Integer.parseInt(tmSimtxt.getText());
         minService = Integer.parseInt(txtMinService.getText());
         maxService = Integer.parseInt(txtMaxService.getText());
          Shop newSh = new Shop(4,0,0,0,minService,maxService,View.this,timeSim); 
        			Thread thr1 = new Thread(newSh);
        			thr1.start();
        		}
        		
        });
	
		
	}
	
	void refreshLists() {
		paneIndex.setPreferredSize(new Dimension(40,200));
		
			pane1.setBounds(0, 100, 780, 200);
			pane2.setBounds(0, 0, 780, 250);
			pane3.setBounds(0, 0, 780, 300);
			pane4.setBounds(0, 0, 780, 300);
			pane1.setPreferredSize(new Dimension(100,200));
	        pane2.setPreferredSize(new Dimension(100,195));
	        pane3.setPreferredSize(new Dimension(100,200));
	        pane4.setPreferredSize(new Dimension(100,200));
			jp.add(paneIndex);
			jp.add(pane1);
			jp.add(pane2);
			jp.add(pane3);
			jp.add(pane4);
			
			
	}
	
	void prepareClients(String[] arr,int id) {
		if(id==1) {
		for(int j=0;j<clienti1.length;j++)
			clienti1[j]=new String();
			
		for(int i=0;i<arr.length;i++)
			clienti1[i]=arr[i];}
		if(id==2) {
			for(int j=0;j<clienti2.length;j++)
				clienti2[j]=new String();
				
			for(int i=0;i<arr.length;i++)
				clienti2[i]=arr[i];}
		if(id==3) {
			for(int j=0;j<clienti3.length;j++)
				clienti3[j]=new String();
				
			for(int i=0;i<arr.length;i++)
				clienti3[i]=arr[i];}
		if(id==4) {
			for(int j=0;j<clienti4.length;j++)
				clienti4[j]=new String();
				
			for(int i=0;i<arr.length;i++)
				clienti4[i]=arr[i];}
		
	}
	
	
public void setClienti(String[] arr,int id) {
	
	prepareClients(arr,id);

	jp.removeAll();
	
	if(id == 1) {
		 pane1.setPreferredSize(new Dimension(100,200));
		clientsQue1 = new JList<String>(clienti1);
		pane1 = new JScrollPane(clientsQue1);}
	
	if(id == 2) {
		 pane2.setPreferredSize(new Dimension(100,200));
		clientsQue2 = new JList<String>(clienti2);
		pane2 = new JScrollPane(clientsQue2);}
	
	if(id == 3) {
		 pane3.setPreferredSize(new Dimension(100,200));
		clientsQue3 = new JList<String>(clienti3);
		pane3 = new JScrollPane(clientsQue3);}
	
	if(id == 4) {
		 pane4.setPreferredSize(new Dimension(100,200));
		clientsQue4 = new JList<String>(clienti4);
		pane4 = new JScrollPane(clientsQue4);}
		
		refreshLists();
		this.validate();
	}
	
    public void setTime(int tm) {
			txtTime.setText(String.valueOf(tm));
		}
	
    public int getTime() {
    	return Integer.parseInt(txtTime.getText());
    }
  
    
	public  int getValueMin() {
		return minService;
	}
	
	public static double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    long factor = (long) Math.pow(10, places);
	    value = value * factor;
	    long tmp = Math.round(value);
	    return (double) tmp / factor;
	}
	
	public int getValueMax() {
		return maxService;
	}
	public void editTextArea(String str) {
		textArea.append(str);
	}
	
	public void setServed(int nr) {
		txtServed.setText(String.valueOf(nr));
	}
	
	public void setAvgWait(double d) {
		avgWait.setText(String.valueOf(round(d,2)));
	}

	  
	  public void setPeakTime(int nr) {
		  peakTxt.setText(String.valueOf(nr));
	  }
	
	public static void main(String[] args) {
		JPanel pn1 = new JPanel();
		 
	  new View(pn1);
	       
     }
}
