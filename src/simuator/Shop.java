package simuator;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

import gui.View;
import java.util.stream.Collectors;



public class Shop implements Runnable {
	private final Object lock = new ReentrantLock();
	View gui ;
	
	 private static ArrayList<ShopQueue> queuesList;
	 private int noQueues ;
	 private static int currentTime;
	 int peakTm =0;
	 int peakVal =0;
	  
	     static int clientsServed = 0;
		 double timeWaited =0;
		 int clientsEntered =0;
		 int minService =1;
		 int maxService =1;
		 int timeSimulation = 1; 
		 Queue<Client> clientsToEntry = new LinkedList<>();
	
	
	public Shop(int nrQ,int tm,int tW, int clientsE, int minS, int maxS,View gi,int timeSim) {
		//initializing the 4 queues(max number admitted by the simulation)
		queuesList = new ArrayList<ShopQueue>();
		for(int i=0;i<nrQ;i++) {
			addNewQueue(0);
			Thread t=new Thread(queuesList.get(i));
			t.start();
		}
		
		
		//initializng the rest of the parameters
		timeSimulation = timeSim;
		currentTime = tm;
     	noQueues = nrQ;
		timeWaited=tW;
		clientsEntered = clientsE;
		gui = gi;
		minService = gi.getValueMin();
		maxService = gi.getValueMax();
	    clientsToEntry = ShopQueue.newClientsGenerator(0,150,minService,maxService);

	  }

	@Override
	public void run() {
      try { 
		while(currentTime < timeSimulation) {
		String[] arr2 = new String[10];
		  if(checkIfFull(queuesList)==false) {
           if(clientsToEntry.peek().getArrivalTime() == currentTime) {
        	  int fsm = insertionInQueue();
				  
  		for(Client c2: queuesList.get(fsm-1).getClientsList()) 
  				timeWaited += c2.getServiceTime(); 
         String str = "Client no. " +  clientsToEntry.peek().getIdClient() + " entries queue " + fsm + " at time " + currentTime +  "\n";
         gui.editTextArea(str);
        clientsToEntry.remove();
  		clientsEntered++;
             }
	}
        int i=0;
			 for(ShopQueue q2: queuesList) {
				 for(Client c1: q2.getClientsList()) {
					arr2[i] = c1.toStr();
					i++;}
					i=0;
					gui.setClienti(arr2, q2.getIdQ());
					arr2= new String[10];
			 }
	if(this.getNoClients() > peakVal) {
	   peakTm = currentTime;
	    peakVal= this.getNoClients();		 
	}
		timeForGui();   // method to display the current time of simulation and compute average waiting time	 
           currentTime++; //increment current time of simulation on each loop
		Thread.currentThread().sleep(1000);}
		   gui.setPeakTime(peakVal);
		   gui.setServed(clientsServed); 
		} 
	catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	int getNrQueues() {
		return noQueues;
	}
	
	
	int insertionInQueue() {
		
		int fsm = Shop.findSmallestQueue(queuesList);
		  clientsToEntry.peek().setFinishTime( processQRes(fsm,queuesList).getClientsList());
		  
		  processQRes(fsm,queuesList).addClient(clientsToEntry.peek());
		  System.out.println("Client " + clientsToEntry.peek().getIdClient() + "inserted in queue no. " + fsm);
		  return fsm;
		
	}
	
	public static void addNewQueue(int currTime ){

		ShopQueue q2 = new ShopQueue(0, 0);
	
		if(queuesList.size()==0)
			q2=new ShopQueue(1,currTime);
		if(queuesList.size()==1)
			q2=new ShopQueue(2,currTime);
	
	
		if(queuesList.size()==2)
		 q2 = new ShopQueue(3,currTime);
		if(queuesList.size()==3)
	     q2= new ShopQueue(4,currTime);
		queuesList.add(q2);
		
	}
	
	static int getCurrTime(){
		return currentTime;
	}
	
	double computeAverage() {
		return timeWaited/clientsServed;
	}
	
	void timeForGui() {
		gui.setTime(currentTime);
        if(clientsEntered >0) {
      	  gui.setAvgWait(computeAverage());
        }
	}
	
	static boolean checkIfFull(ArrayList<ShopQueue> q1) {
		boolean ok = true;
		for(ShopQueue QQ : q1) {
			if(QQ.getClientsList().size() < 10)
				ok=false;
		}
    return ok;
}
	
	static ShopQueue processQRes(int res,ArrayList<ShopQueue> list) {
		if(res==1)
			return list.get(0);
		if(res==2)
			return list.get(1);
		if(res==3)
			return list.get(2);
		if(res==4)
			return list.get(3);
		
		return list.get(0);
	   }
	
	private static int findSmallestQueue(ArrayList<ShopQueue> lst ) {
		int id=1;
		int min=999;
		for(ShopQueue q1: lst) {
			if(q1.getClientsList().size()<=min) {
				id=q1.getIdQ();
				min=q1.getClientsList().size();
		     }
	    }
			return id;
	}
	
	public static int getClientsServed() {
		return clientsServed;
	}
	
	public static void setClientsServed(int nr) {
		clientsServed = nr;
	}
	
	public int getNoClients() {
		int res=0;
		for(ShopQueue q1 : queuesList) {
			for(Client c1 : q1.getClientsList())
				res++;
		}
		return res;
	}
	
	static int getRandomQueue(int max) {
		int n=1;
            Random r = new Random();
			n= r.nextInt(max) + 1;
       return n;
	    }
}
	

