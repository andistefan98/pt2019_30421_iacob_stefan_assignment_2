package simuator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
public class ShopQueue implements Runnable{
	
	private BlockingQueue<Client> queue1=new ArrayBlockingQueue<Client>(100);
	private int idQ=1;

	int currTime =0;

	public ShopQueue( int id,int current) {
		idQ = id;
		
		currTime = current;
	}
	
	@Override
	public void run() {

	try {
		while(true) {
	
			Client c=exitClient();
			Shop.setClientsServed(Shop.getClientsServed()+1);
			System.out.println(" Starting processing for clint no." +  " w/ Id " + c.getIdClient() + " in queue " + idQ + " with waiting time " + c.getServiceTime() );
			Thread.sleep(1000 * c.getServiceTime());
	
		}
	}
	catch(InterruptedException ex) {
		ex.getMessage();
	   }
		    
  }
	
	public synchronized Client exitClient() throws InterruptedException {
	//System.out.println(" BEEN IN EXIT");
		while(queue1.size()==0)
			wait();
		Client c=queue1.take();
		notifyAll();
		return c;
	}

	
	public Queue<Client> getClientsList(){
		return queue1;
	}
	
	
	
	String toSt(int nr) {
		String str = String.valueOf(nr);
		return str;
	}
	
	

	synchronized void  addClient(Client c) {
		queue1.add(c);
		notifyAll();
	}
	

	int getIdQ() {
		return idQ;
	}

	
	  public static Queue<Client> newClientsGenerator(int tCurrent,int max_nr,int min,int max) {
		  Queue<Client> clientsList = new LinkedList<Client>();
		  int id=1;
		  int delta=1;
		  while(max_nr !=0 ) {
			///int timeC = 0;
			Client c1 = new Client(0,0,0);
			
				int nr = c1.calculateService(min,max);
				c1.setServiceTime(nr);
				
				Random rand = new Random();
				c1.setArrivalTime(rand.nextInt(1) + tCurrent);	
				
				c1.setIdClient(id);
				clientsList.add(c1);
				
				tCurrent= tCurrent + delta;
				id++;
				max_nr--;
		   }
		   return clientsList; 
		}

	
}
