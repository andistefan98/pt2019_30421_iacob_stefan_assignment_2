package simuator;
import java.awt.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

public class Client {
	
	 private int arrivalTime;
	  private int finishTime;
	  private int serviceTime; 
	 private  int idClient;
	  
	  public Client(int arr, int serv,int id) {
		  arrivalTime = arr;
		  serviceTime=serv;
		  idClient = id ;
	  }
	  
	  int calculateService(int low, int high) {
		  Random rand = new Random() ;
		  int result = rand.nextInt(high-low) + low;
		  return result;
	  }
	  
	  
	  void setIdClient(int id) {
		  idClient = id;
	  }
	  
	  public int getIdClient() {
		  return idClient;
	  }
	  
	  public int getServiceTime() {
		  return serviceTime;
	  }
	  
	  void setArrivalTime(int tim) {
		  arrivalTime = tim; 
	  }
	  
	  void setServiceTime(int t) {
		   serviceTime = t;
	  }
	  
	  void setFinishTime(Queue<Client> q1) {
		  finishTime = arrivalTime + getWaitingTime(q1);
		  
	  }
	  
	  int getArrivalTime() {
		  return arrivalTime;
	  }

	  int getWaitingTime(Queue<Client> q1) {
		  int tmW =0;
		  if(q1.peek() != this)
			  return -1;
		  
		 for(Client c1: q1) {
			 tmW += c1.serviceTime;
		 }
		 return tmW;
	  }
	  
	  int getExpectedFinish(Queue<Client> q1) {
		  return (getWaitingTime(q1) + arrivalTime);
	  }
	
	
	
	  
	  public String toStr() {
		  String str;
		   str="("+String.valueOf(serviceTime) + "  ,  " + String.valueOf(idClient)+"";
		  return str;
	    }
	  
	
  }
